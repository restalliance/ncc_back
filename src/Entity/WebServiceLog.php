<?php

// src/Entity/WebServiceLog.php
namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * @ApiResource
 * @ORM\Entity
 */
class WebServiceLog // The class name will be used to name exposed resources
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @param string $user A User property - this description will be avaliable in the API documentation too.
     *
     * @ORM\Column
     */
    public $user;


    /**
     * @param string $httpaction Http Actions
     * 
     * @ORM\Column
     * @Assert\NotBlank
     */
    public $httpaction;

    /**
     * @param string $url  Url Lauching
     * 
     * @ORM\Column
     * @Assert\NotBlank
     */
    public $url;

    /**
     * @param string $result  Result of HTTP Request
     * 
     * @ORM\Column
     * @Assert\NotBlank
     */
    public $result;

    /**
     * @param string $httpreturncode  Result Code of HTTP Request
     * 
     * @ORM\Column
     * @Assert\NotBlank
     */
    public $httpreturncode;


    /**
     * @param string $type  Type of Log
     * 
     * @ORM\Column
     */
    public $type;

    /**
     * @param datetime $timestamp Timestamp of Insert
     * 
     * @ORM\Column
     * @Assert\NotBlank
     */
    public $timestamp;
}